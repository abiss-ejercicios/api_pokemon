const { Router } = require('express');

const { pokemonController } = require('../controllers');

const router = Router();

router.get('/', pokemonController.search);

module.exports = router;
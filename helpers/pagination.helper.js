const skip = (list = [], start = 0) => {

    const result = [];
    list.forEach((l, index) => {
        
        if ( index >= start ) {

            result.push(l);
        }
    });

    return result.filter(() => true);
};

const take = (list = [], count = 0) => {

    const result = [];
    for (let i = 0; i < count; i++) {
        
        if (!list[i]) { break; }

        result.push(list[i]);
    }

    return result.filter(() => true);
};

const pagination = (list = [], page = 1, size = 0) => {

    const skipList = skip(list, (page - 1) * size );
    return take(skipList, size);
}

module.exports = {
    skip,
    take,
    pagination
}
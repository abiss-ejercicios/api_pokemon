const { response, request } = require('express');
const { pokemonService } = require('../services');

const maxLengthSearch = process.env.MAX_SEARCH;

const search = async (req = request, res = response) => {

    const { q, page, size } = req.query;

    // si se excede en caracteres en el campo búsqueda
    if (q.length > maxLengthSearch) {

        return res.status(400).json({

            msg: `Se permiten hasta ${ maxLengthSearch } caracteres para la búsqueda`
        });
    }

    const pokemons = await pokemonService.search(q, page, size);

    res.json({
        pokemons
    });
}

module.exports = {
    search
}
const axios = require('axios');
const { paginationHelper } = require('../helpers');

const instance = axios.create({
    baseURL: `${process.env.API_URL}`
});

const search = async (search, page, size) => {

    try {
        
        // No encontré para esta api un endpoint que me permita buscar por texto parcial
        // Por eso me traigo toda la lista disponible (que no es mucho) y la parseo de este lado
        const { data: { results, count } } = await instance.get('/pokemon', {
            params: {
                offset: 0,
                limit: process.env.PAGINATION_SIZE
            }
        });

        const matching = results.filter(({ name }) => name.toUpperCase().includes(search.toUpperCase()));
        const listPagination = paginationHelper.pagination(matching, page, size);

        return {
            results: listPagination.map(l => {

                const sections = l.url.split('/');
                const id = sections[sections.length - 2];

                return {
                    id,
                    ...l,
                    url: `${process.env.URL_IMAGE.replace('replace', id)}`
                };
            }),
            total: count
        }

    } catch (error) {
        
        throw new Error(error);
    }
}

module.exports = {

    search
}
# RestServer

1. Ejecutar ```npm install``` para construir los módulos de Node.

2. Asegurarse de contar con estas varaibles de entorno

PORT=8080
API_URL=https://pokeapi.co/api/v2
URL_IMAGE=https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/replace.png
PAGINATION_SIZE: 1200
MAX_SEARCH=100

3. Ejecutar con ```nodemon app``` o ```npm start```